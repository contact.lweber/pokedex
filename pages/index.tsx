import { Container } from '@chakra-ui/react';
import { useTheme } from '@emotion/react';
import { Form, Global, Layout } from 'components';

function Home() {
    const theme = useTheme();
    return (
        <Global>
            <Container
                as={Layout}
                rounded="lg"
                bg={theme.colors.darkenWhite}
                maxWidth="300px"
            >
                <Form />
            </Container>
        </Global>
    );
}

export default Home;

import theme from 'lib/theme';
import type { AppProps } from 'next/app';
import { ThemeProvider, useTheme } from '@emotion/react';

import emotionReset from 'emotion-reset';
import { Global } from '@emotion/react';
import { ChakraProvider } from '@chakra-ui/react';
import NextNprogress from 'nextjs-progressbar';

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <ChakraProvider>
            <NextNprogress
                options={{ easing: 'ease', speed: 500 }}
                height={5}
                color="#264653"
            />
            <ThemeProvider theme={theme}>
                <Global styles={emotionReset} />

                <Component {...pageProps} />
            </ThemeProvider>
        </ChakraProvider>
    );
}

export default MyApp;

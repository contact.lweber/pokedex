import { Error, Global } from 'components';

export default function Custom404() {
    return (
        <Global>
            <Error codeError="404" />
        </Global>
    );
}

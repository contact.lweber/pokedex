import styled from '@emotion/styled';
import Document, { Head, Html, Main, NextScript } from 'next/document';

const Body = styled.body`
    font-family: 'Poppins', sans-serif;
`;

class AppDocument extends Document {
    private setMetaElements() {
        return (
            <>
                <meta charSet="UTF-8" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
                <meta name="description" content="Pokédex" />
                <meta name="apple-mobile-web-app-title" content="Pokédex" />
                <meta name="application-name" content="Pokédex" />

                <meta
                    name="msapplication-config"
                    content="/browserconfig.xml"
                />

                <noscript>
                    Oh no, you either have JavaScript turned off or your browser
                    doesn&apos;t support JavaScript
                </noscript>
                <noscript>
                    Oh non, soit JavaScript est désactivé, soit votre navigateur
                    ne prend pas en charge JavaScript
                </noscript>

                <script
                    async
                    defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC15LjQm5fhjXIIj401xLocYlRnoJB3cmY&language=fr&libraries=places"
                />
            </>
        );
    }

    private setLinkElements() {
        return (
            <>
                <link rel="manifest" href="/manifest.json" />

                <link rel="preconnect" href="https://fonts.googleapis.com" />
                <link
                    rel="preconnect"
                    href="https://fonts.gstatic.com"
                    crossOrigin="true"
                />
                <link
                    href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,400;0,600;0,700;1,100;1,300;1,400;1,600;1,700&display=swap"
                    rel="stylesheet"
                />
            </>
        );
    }

    render() {
        return (
            <Html lang={this.context.defaultLocale}>
                <Head>
                    {this.setMetaElements()}
                    {this.setLinkElements()}
                </Head>
                <Body>
                    <Main />
                    <NextScript />
                </Body>
            </Html>
        );
    }
}

export default AppDocument;

import { Error, Global } from 'components';

export default function Custom500() {
    return (
        <Global>
            <Error codeError="500" />
        </Global>
    );
}

import { Global, Layout, PokemonDetails } from 'components';
import React from 'react';
import axios from 'axios';
import type { PokemonType } from 'lib/types/pokemon';
import type { GetServerSideProps } from 'next';
import { Container } from '@chakra-ui/react';

function Pokemon({ pokemon }: { pokemon: PokemonType }) {
    return (
        <Global>
            <Container as={Layout} p="0" maxWidth="1300px">
                <PokemonDetails {...pokemon} />
            </Container>
        </Global>
    );
}

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
    const response = await axios.get(
        `https://pokeapi.co/api/v2/pokemon/${params?.pokemonId}/`
    );
    return {
        props: {
            pokemon: response.data,
        },
    };
};

export default Pokemon;

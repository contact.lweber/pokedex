import '@emotion/react';
import { ThemeType } from './lib/theme/type';

declare module '@emotion/react' {
    export interface Theme extends ThemeType {}
}

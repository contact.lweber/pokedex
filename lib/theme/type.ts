export type ThemeType = {
    colors: Record<ColorsType, string>;
    font: Record<SizesType, string>;
    sizes: Record<SizesType, string>;
};

export type ColorsType =
    | 'normal'
    | 'fire'
    | 'water'
    | 'grass'
    | 'electric'
    | 'ice'
    | 'fight'
    | 'poison'
    | 'ground'
    | 'flying'
    | 'psychic'
    | 'bug'
    | 'rock'
    | 'ghost'
    | 'dark'
    | 'dragon'
    | 'steel'
    | 'fairy'
    | 'darkenWhite'
    | 'black';

type SizesType = 'xxs' | 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl' | 'xxxl';

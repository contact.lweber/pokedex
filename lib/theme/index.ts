import { ThemeType } from './type';

const theme = {
    colors: {
        normal: '#A8A87B',
        fire: '#EE803B',
        water: '#6A92ED',
        grass: '#7BC757',
        electric: '#F7CF43',
        ice: '#9AD8D8',
        fight: '#BE322E',
        poison: '#9B4A99',
        ground: '#DFBF6E',
        flying: '#A893ED',
        psychic: '#EC5B89',
        bug: '#A8B732',
        rock: '#B89F41',
        ghost: '#705A97',
        dark: '#705849',
        dragon: '#7043F4',
        steel: '#B8B9CF',
        fairy: '#EFB7BD',
        darkenWhite: '#EDE3DC',
        black: '#264653',
    },
    sizes: {
        xxs: '5px',
        xs: '10px',
        sm: '15px',
        md: '20px',
        lg: '30px',
        xl: '40px',
        xxl: '50px',
        xxxl: '60px',
    },
    font: {
        xxs: '5px',
        xs: '10px',
        sm: '14px',
        md: '16px',
        lg: '24px',
        xl: '30px',
        xxl: '40px',
        xxxl: '50px',
    },
} as ThemeType;

export default theme;

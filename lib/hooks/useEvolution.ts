import axios from 'axios';
import { EvolutionTypes } from 'lib/types/evolution';
import React from 'react';

export default function useEvolution(name: string) {
    const [evolutions, setEvolutions] = React.useState<EvolutionTypes>();
    const [isLoading, setIsLoading] = React.useState(false);

    async function fetchEvolutions() {
        try {
            setIsLoading(true);
            const response = await axios.get(
                `https://pokeapi.co/api/v2/pokemon-species/${name}/`
            );
            if (response.data) {
                const resEvolv = await axios.get(
                    response.data.evolution_chain.url
                );
                if (resEvolv) {
                    setEvolutions(resEvolv.data);
                    setIsLoading(false);
                } else {
                    setEvolutions(undefined);
                    setIsLoading(false);
                }
            }
        } catch (error) {
            setIsLoading(false);
            throw new Error('Error occured');
        }
    }

    React.useEffect(() => {
        fetchEvolutions();
    }, []);
    return { evolutions, isLoading };
}

function chainEvolvutions(
    aggr: string[],
    evolution: any | undefined
): string[] {
    const currentName = evolution?.species.name;
    const concatName = [...aggr, currentName];
    if (evolution && evolution?.evolves_to.length > 0) {
        return chainEvolvutions(concatName, evolution?.evolves_to[0]);
    }
    return concatName;
}

export default chainEvolvutions;

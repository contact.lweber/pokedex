import { getRandom } from '..';

describe('get random', () => {
    it('should render random number', () => {
        const randomNumber = getRandom([1, 2, 3]);
        expect(randomNumber).toEqual(expect.any(Number));
    });

    it('should throw error if params is not a number []', () => {
        expect(() => {
            //@ts-ignore
            getRandom('test');
        }).toThrowError('params must be array of numbers');
    });
});

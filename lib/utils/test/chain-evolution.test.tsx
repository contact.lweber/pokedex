import chainEvolvutions from '../chainEvolvutions';

const mock = {
    evolution_details: [],
    evolves_to: [
        {
            evolves_to: [
                {
                    evolves_to: [],
                    species: {
                        name: 'poliwrath',
                        url: 'https://pokeapi.co/api/v2/pokemon-species/62/',
                    },
                },
                {
                    evolves_to: [],
                    species: {
                        name: 'politoed',
                        url: 'https://pokeapi.co/api/v2/pokemon-species/186/',
                    },
                },
            ],
            species: {
                name: 'poliwhirl',
                url: 'https://pokeapi.co/api/v2/pokemon-species/61/',
            },
        },
    ],
    species: {
        name: 'poliwag',
        url: 'https://pokeapi.co/api/v2/pokemon-species/60/',
    },
};

describe('chain evolution test', () => {
    it('should chain evolves_to to build string[] of evolution', () => {
        const allEvolv = chainEvolvutions([], mock);
        expect(allEvolv).toEqual(
            expect.arrayContaining(['poliwag', 'poliwhirl', 'poliwrath'])
        );
    });
});

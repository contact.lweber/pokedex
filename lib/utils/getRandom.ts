import { isArray } from 'util';

function getRandom(list: number[]) {
    if (list.constructor !== Array) {
        throw new Error('params must be array of numbers');
    } else {
        return list[Math.floor(Math.random() * list.length)];
    }
}

export default getRandom;

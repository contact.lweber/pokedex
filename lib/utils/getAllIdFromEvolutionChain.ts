function getAllIdFromEvolutionChain(
    aggr: string[],
    evolution: any | undefined
): string[] {
    const currentId = evolution?.species.url
        .split('species/')[1]
        .replace('/', '');
    const concatId = [...aggr, currentId];
    if (evolution && evolution?.evolves_to.length > 0) {
        return getAllIdFromEvolutionChain(concatId, evolution?.evolves_to[0]);
    }
    return concatId;
}

export default getAllIdFromEvolutionChain;

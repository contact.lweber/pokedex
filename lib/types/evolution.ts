export type EvolutionTypes = {
    id: number;
    baby_trigger_item: string | null;
    chain: {
        is_baby: boolean;
        species: {
            name: string;
            url: string;
        };
        evolution_details: string | null;
        evolves_to: EvolvesTo[];
    };
};

type EvolvesTo = {
    is_baby: boolean;
    species: {
        name: string;
        url: string;
    };
    evolution_details: [
        {
            item: string | null;
            trigger: {
                name: string;
                url: string;
            };
            gender: string | null;
            held_item: string | null;
            known_move: string | null;
            known_move_type: string | null;
            location: string | null;
            min_level: 20;
            min_happiness: string | null;
            min_beauty: string | null;
            min_affection: string | null;
            needs_overworld_rain: boolean;
            party_species: string | null;
            party_type: string | null;
            relative_physical_stats: string | null;
            time_of_day: string;
            trade_species: string | null;
            turn_upside_down: boolean;
        }
    ];
    evolves_to: EvolvesTo[];
};

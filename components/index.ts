// PURE COMPONENTS
export { default as Button } from './Button';
export { default as Container } from './Container';
export { default as Global } from './GlobalLayout';
export { default as Layout } from './Layout';
export { default as Spinner } from './Spinner';
export { default as Stack } from './Stack';

// COMPONENTS FUNCTION FOR PAGES
export { default as Error } from './Error';
export { default as Form } from './Form';
export { default as Informations } from './pokemonDetails/Information';
export { default as PokemonDetails } from './pokemonDetails';
export { default as Stats } from './Stats';

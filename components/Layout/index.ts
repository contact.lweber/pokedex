import styled from '@emotion/styled';

type LayoutProps = {
    maxWidth?: string;
    hasPadding?: boolean;
};

const Layout = styled.section<LayoutProps>`
    width: ${({ maxWidth }) => (maxWidth ? `${maxWidth}px` : '300px')};
    background: ${({ theme }) => theme.colors.darkenWhite};
    border-radius: ${({ theme }) => theme.sizes.sm};
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: ${({ theme, hasPadding }) => (hasPadding ? theme.sizes.md : '0')}
        ${({ theme, hasPadding }) => (hasPadding ? theme.sizes.xxxl : '0')};
`;

export default Layout;

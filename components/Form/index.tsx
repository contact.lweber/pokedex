import { Button, Stack } from 'components';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import { FormComp, Header, SubTitle, Error, Footer } from './components';

import { getRandom } from 'lib/utils';
import { RandomIcon } from 'components/Icons';
import { Input } from '@chakra-ui/react';
import { useTheme } from '@emotion/react';

export default function Form() {
    const [value, setValue] = React.useState('');
    const [errors, setError] = React.useState(false);
    const router = useRouter();
    function searchPokemon(id: string) {
        if (id.length <= 0) setError(true);
        else router.push(`/pokemon/${id}`);
    }

    const theme = useTheme();

    return (
        <FormComp
            onSubmit={(e) => {
                e.preventDefault();
                searchPokemon(value);
            }}
        >
            <Stack spacing="20" style={{ margin: '20px 0' }}>
                <Header>
                    <Image
                        alt="Logo de la pokéball"
                        width="75px"
                        height="75px"
                        src="/img/abstracts/pokeball.svg"
                    />
                </Header>

                <Stack spacing="5">
                    <SubTitle>Pokemon name or id</SubTitle>
                    <Input
                        fontWeight={700}
                        color={theme.colors.black}
                        fontSize={theme.sizes.md}
                        textTransform="uppercase"
                        border="none"
                        bg="white"
                        onInput={(e) => {
                            setError(false);
                            setValue(e.currentTarget.value);
                        }}
                        type="text"
                    />
                    {errors && <Error>Merci de renseigner le champ</Error>}
                </Stack>

                <Footer>
                    <Button type="submit">Search !</Button>
                    <Button
                        isRounded
                        onClick={(e) => {
                            e.preventDefault();
                            router.push(
                                `/pokemon/${getRandom(
                                    Array.from(Array(151).keys())
                                )}`
                            );
                        }}
                    >
                        <RandomIcon />
                    </Button>
                </Footer>
            </Stack>
        </FormComp>
    );
}

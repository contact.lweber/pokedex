import styled from '@emotion/styled';

const Header = styled.header`
    text-align: center;
`;

const FormComp = styled.form`
    width: 100%;
`;

const SubTitle = styled.h1`
    font-size: ${({ theme }) => theme.font.sm};
    text-transform: uppercase;
    color: ${({ theme }) => theme.colors.black};
    font-weight: 600;
`;

const Input = styled.input`
    border: none;
    border-radius: ${({ theme }) => theme.sizes.xxs};
    padding: ${({ theme }) => theme.sizes.xs};
    width: -webkit-fill-available;
    text-transform: uppercase;
    font-size: ${({ theme }) => theme.sizes.md};
    color: ${({ theme }) => theme.colors.black};
    font-weight: 700;
`;

const Footer = styled.footer`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
`;

const Error = styled.p`
    color: ${({ theme }) => theme.colors.fire};
    font-size: ${({ theme }) => theme.font.sm};
`;

export { Header, FormComp, SubTitle, Input, Footer, Error };

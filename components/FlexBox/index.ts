import styled from '@emotion/styled';

const FlexRowCenter = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
`;

const FlexColumnCenter = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    flex-wrap: wrap;
`;

export { FlexRowCenter, FlexColumnCenter };

import React from 'react';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';

import type { PokemonType } from 'lib/types/pokemon';

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);
import { Bar } from 'react-chartjs-2';
import { StatsComp } from './components';

const labels = ['HP', 'Attack', 'Defense', 'Spe A', 'Spe D', 'Speed'];

export default function Stats(props: PokemonType) {
    const stats = props.stats.reduce((aggr, curr) => {
        if (curr.base_stat) return [...aggr, curr.base_stat];
        else return aggr;
    }, [] as number[]);

    const data = {
        labels,
        datasets: [
            {
                label: 'Stats',
                data: stats,
                backgroundColor: '#38A8D5',
            },
        ],
    };
    return (
        <StatsComp>
            <Bar
                options={{
                    maintainAspectRatio: false,
                    responsive: true,
                    scales: {
                        y: { ticks: { display: false } },
                    },
                }}
                data={data}
            />
        </StatsComp>
    );
}

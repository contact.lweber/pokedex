import styled from '@emotion/styled';

const StatsComp = styled.section`
    background: white;
    border: 4px solid #c4c4c4;
    @media (max-width: 500px) {
        padding: 10px;
    }
    @media (min-width: 501px) {
        padding: 30px;
    }
    canvas {
        max-width: 450px;
    }
`;

export { StatsComp };

import styled from '@emotion/styled';

const Container = styled.section`
    padding: ${({ theme }) => theme.sizes.md} ${({ theme }) => theme.sizes.xl};
    margin: 0 auto;
`;

export default Container;

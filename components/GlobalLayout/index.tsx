import { Layout } from 'components';
import { motion } from 'framer-motion';
import { PropsWithChildren } from 'react';
import GlobalLayout from './components';

export default function Global({
    children,
}: {
    children: PropsWithChildren<unknown>;
}) {
    return (
        <GlobalLayout>
            <motion.div
                style={{
                    marginInlineStart: '0',
                    width: '100%',
                }}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ type: 'keyframes', delay: 0.1 }}
            >
                {children}
            </motion.div>
        </GlobalLayout>
    );
}

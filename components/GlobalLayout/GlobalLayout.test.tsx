import { render } from '@testing-library/react';
import theme from '../../lib/theme/';

import GlobalLayout from './components';

describe('GlobalLayout', () => {
    it('should set min height to 100vh', () => {
        const { getByTestId } = render(
            <GlobalLayout data-testid="global-layout" theme={theme} />
        );

        expect(getByTestId('global-layout')).toHaveStyle('min-height: 100vh');
    });
});

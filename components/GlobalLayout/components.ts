import styled from '@emotion/styled';

const GlobalLayout = styled.section`
    min-height: 100vh;
    background: linear-gradient(180deg, #f4a261 0%, #141a2b 100%);
    display: flex;
    align-items: center;
    justify-content: center;
    padding: ${({ theme }) => theme.sizes.lg};
`;

export default GlobalLayout;

import styled from '@emotion/styled';
import { ColorsType } from 'lib/theme/type';

type ButtonTypes = {
    background?: ColorsType;
    isRounded?: boolean;
};

const Button = styled.button<ButtonTypes>`
    outline: none;
    border: none;
    padding: ${({ theme, isRounded }) => (isRounded ? '0' : theme.sizes.xxs)}
        ${({ theme, isRounded }) => (isRounded ? 'O' : theme.sizes.sm)};
    border-radius: ${({ isRounded, theme }) =>
        isRounded ? '100%' : theme.sizes.xs};
    background: ${({ background, theme }) =>
        background ? theme.colors[background] : '#F4A261'};
    color: white;
    font-size: ${({ theme }) => theme.font.md};
    font-weight: 600;
    cursor: pointer;
    &:hover,
    &:focus {
        filter: brightness(90%);
    }
    &:focus {
        border: 2px solid ${({ theme }) => theme.colors.black};
    }
    min-width: 40px;
    min-height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 ${({ theme }) => theme.sizes.xxs};
`;

export default Button;

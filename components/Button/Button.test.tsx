import Button from './index';
import { render, cleanup, screen } from '@testing-library/react';

import theme from '../../lib/theme/index';

afterEach(cleanup);

describe('Button', () => {
    it('should render button correctly', () => {
        const { getByRole } = render(<Button theme={theme} />);
        const button = getByRole('button');
        expect(button).toBeInTheDocument();
    });

    it('should render correct color button', () => {
        const { getByRole } = render(
            <Button theme={theme} background="ghost" />
        );
        const button = getByRole('button');

        expect(button).toHaveStyle('background-color: #705A97');
    });

    it('should render children text', () => {
        const { getByText } = render(<Button theme={theme}>Focus me!</Button>);
        expect(getByText('Focus me!')).toBeInTheDocument();
    });

    it('should make a default color if no color provided', () => {
        const { getByRole } = render(
            <Button theme={theme}>No color provided</Button>
        );
        const button = getByRole('button');

        expect(button).toHaveStyle('background-color: #F4A261');
    });
});

import { Text, VStack } from '@chakra-ui/react';
import { useTheme } from '@emotion/react';
import { Button } from 'components';
import Link from 'next/link';

export default function Error({ codeError }: { codeError: '404' | '500' }) {
    const theme = useTheme();
    return (
        <VStack
            m="0 auto"
            maxW="500px"
            bg="white"
            borderRadius="lg"
            spacing="4"
            p="10"
        >
            <Link href="/" passHref>
                <Button>
                    <a>Retour</a>
                </Button>
            </Link>
            <Text color={theme.colors.fire}>
                {codeError === '500' ? (
                    <p>
                        Une erreur est survenue, merci de réessayer
                        ultérieurement.
                    </p>
                ) : (
                    <p> La page demandée n&apos;a pas été trouvée</p>
                )}
            </Text>
        </VStack>
    );
}

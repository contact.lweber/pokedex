import { Stack } from 'components';
import { PokemonType } from 'lib/types/pokemon';
import {
    InformationsBloc,
    InformationsComp,
    SimpleText,
    Value,
} from './components';

export default function Informations(props: PokemonType) {
    return (
        <InformationsComp>
            <Stack spacing="30">
                <InformationsBloc>
                    <SimpleText>Weight</SimpleText>
                    <Value>{props.weight}kg</Value>
                </InformationsBloc>
                <InformationsBloc>
                    <SimpleText>Height</SimpleText>
                    <Value>{props.height}m</Value>
                </InformationsBloc>
                <InformationsBloc>
                    <SimpleText>Abilities</SimpleText>
                    {props.abilities?.map((ability, index) => (
                        <>
                            <Value key={index}>
                                {ability.ability.name}
                                {index !== props?.abilities?.length - 1 && ', '}
                            </Value>
                        </>
                    ))}
                </InformationsBloc>
            </Stack>
        </InformationsComp>
    );
}

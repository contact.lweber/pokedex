import styled from '@emotion/styled';

const Header = styled.header`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: ${({ theme }) => theme.sizes.lg} ${({ theme }) => theme.sizes.xl};
    height: 100%;
    border-bottom: ${({ theme }) => `1px solid ${theme.colors.fire}`};
`;

const HeaderTitle = styled.h2`
    color: ${({ theme }) => theme.colors.black};
    text-transform: uppercase;
    font-weight: 700;
    font-size: ${({ theme }) => theme.font.xl};
`;

const SubTitle = styled(HeaderTitle)`
    font-size: ${({ theme }) => theme.font.md};
`;

const InformationsComp = styled.article`
    box-shadow: 0px 2px 4px 0px #00000040;
    border-radius: ${({ theme }) => theme.sizes.lg};
    padding: ${({ theme }) => theme.sizes.lg};
    background: #fff;
    max-width: 400px;
`;

const InformationsBloc = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
`;

const SimpleText = styled.h3`
    color: ${({ theme }) => theme.colors.black};
    font-weight: 700;
    padding-right: ${({ theme }) => theme.sizes.lg};
`;
const Value = styled.h3`
    color: #e76f51;
    font-weight: 700;
`;
export {
    Header,
    HeaderTitle,
    InformationsComp,
    SimpleText,
    Value,
    InformationsBloc,
    SubTitle,
};

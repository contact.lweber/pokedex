import React from 'react';
import Link from 'next/link';
import Image from 'next/image';

import { Header, HeaderTitle, SubTitle } from './components';
import { Button, Container, Spinner, Stats } from 'components';
import { ChevronLeftIcon, ChevronRightEvolution } from 'components/Icons';
import { FlexColumnCenter, FlexRowCenter } from 'components/FlexBox';

import Informations from './Information';
import useEvolution from 'lib/hooks/useEvolution';
import { chainEvolvutions, getAllIdFromEvolutionChain } from 'lib/utils';

import type { PokemonType } from 'lib/types/pokemon';
import type { ColorsType } from 'lib/theme/type';
import { Box, Img, SimpleGrid } from '@chakra-ui/react';

export default function PokemonDetails(props: PokemonType) {
    const { evolutions, isLoading } = useEvolution(props.name);
    const allEvolvs = chainEvolvutions([], evolutions?.chain);
    const evolutionsInformations = getAllIdFromEvolutionChain(
        [],
        evolutions?.chain
    );

    return (
        <Box w="100%">
            {isLoading ? (
                <Box p="20">
                    <Spinner />
                </Box>
            ) : (
                <>
                    <Header>
                        <Link href="/">
                            <a>
                                <ChevronLeftIcon />
                            </a>
                        </Link>
                        <HeaderTitle>
                            {props.name || 'Aucun résultat'}
                        </HeaderTitle>
                        <div></div>
                    </Header>
                    <Container>
                        <SimpleGrid
                            alignItems="center"
                            columns={[1, 2, 3]}
                            flexWrap="wrap"
                            spacing="6"
                        >
                            <Stats {...props} />

                            <Box m="0 auto">
                                <Img
                                    maxW="200px"
                                    src={
                                        props.sprites.other.dream_world
                                            .front_default
                                    }
                                    alt={props.name}
                                />
                                <FlexRowCenter>
                                    {props.types.map(({ type }, index) => (
                                        <Button
                                            style={{ marginTop: '10px' }}
                                            key={index}
                                            background={type.name as ColorsType}
                                        >
                                            {type.name}
                                        </Button>
                                    ))}
                                </FlexRowCenter>
                            </Box>
                            <Informations {...props} />
                        </SimpleGrid>
                        <FlexRowCenter>
                            {evolutionsInformations.map((svg, i) => (
                                <FlexRowCenter key={i}>
                                    <FlexColumnCenter
                                        style={{ padding: '50px' }}
                                        key={i}
                                    >
                                        <Link href={`/pokemon/${svg}`}>
                                            <a href="">
                                                <Image
                                                    alt="Image du pokémon"
                                                    width="100px"
                                                    height="100px"
                                                    src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${svg}.svg`}
                                                />
                                            </a>
                                        </Link>
                                        <SubTitle
                                            style={{ paddingTop: '20px' }}
                                        >
                                            {allEvolvs[i]}
                                        </SubTitle>
                                    </FlexColumnCenter>
                                    {i !==
                                        evolutionsInformations.length - 1 && (
                                        <div style={{ display: 'flex' }}>
                                            <ChevronRightEvolution
                                                color={
                                                    props.types[0]?.type.name
                                                }
                                            />
                                            <ChevronRightEvolution
                                                color={
                                                    props.types[0]?.type.name
                                                }
                                            />
                                        </div>
                                    )}
                                </FlexRowCenter>
                            ))}
                        </FlexRowCenter>
                    </Container>
                </>
            )}
        </Box>
    );
}

import styled from '@emotion/styled';

type SpacingType = {
    spacing: string;
};

const Stack = styled.div<SpacingType>`
    width: 100%;
    > *:not(:first-child) {
        margin-top: ${({ spacing }) => `${spacing}px`};
    }
`;

export default Stack;
